package com.example.tubes_new

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class SejarahIndMenggugat : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sejarah_ind_menggugat)

        val idmenggugatMap = findViewById<Button>(R.id.btnINDMap)
        idmenggugatMap.setOnClickListener {
            val gmmIntentUri = Uri.parse("geo:0,0?q=Gedung Indonesia Menggugat")
            val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
            mapIntent.setPackage("com.google.android.apps.maps")
            startActivity(mapIntent)
        }
    }
}