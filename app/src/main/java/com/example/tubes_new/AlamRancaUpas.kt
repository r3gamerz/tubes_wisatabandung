package com.example.tubes_new

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class AlamRancaUpas : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_alam_ranca_upas)

        val rancaupasMap = findViewById<Button>(R.id.btnRCUMap)
        rancaupasMap.setOnClickListener {
            val gmmIntentUri = Uri.parse("geo:0,0?q=Rancaupas")
            val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
            mapIntent.setPackage("com.google.android.apps.maps")
            startActivity(mapIntent)
        }
    }
}