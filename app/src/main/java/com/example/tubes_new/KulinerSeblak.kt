package com.example.tubes_new

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class KulinerSeblak : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_kuliner_seblak)

        val seblakMap = findViewById<Button>(R.id.btnSBLMap)
        seblakMap.setOnClickListener {
            val gmmIntentUri = Uri.parse("geo:0,0?q=Seblak Basah Deu Tjeunghar")
            val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
            mapIntent.setPackage("com.google.android.apps.maps")
            startActivity(mapIntent)
        }
    }
}