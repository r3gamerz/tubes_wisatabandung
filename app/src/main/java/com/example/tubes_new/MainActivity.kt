package com.example.tubes_new

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val pageKuliner = findViewById<Button>(R.id.btnPageKuliner)
        val pageAlam = findViewById<Button>(R.id.btnPageAlam)
        val pageBelanja = findViewById<Button>(R.id.btnPageBelanja)
        val pageSejarah = findViewById<Button>(R.id.btnPageSejarah)

        pageKuliner.setOnClickListener {
        val intent = Intent(this,PageKuliner::class.java)
        startActivity(intent)
        }
        pageAlam.setOnClickListener {
            val intent = Intent(this,PageAlam::class.java)
            startActivity(intent)
        }
        pageBelanja.setOnClickListener {
            val intent = Intent(this,PageBelanja::class.java)
            startActivity(intent)
        }
        pageSejarah.setOnClickListener {
            val intent = Intent(this,PageSejarah::class.java)
            startActivity(intent)
        }
    }

}