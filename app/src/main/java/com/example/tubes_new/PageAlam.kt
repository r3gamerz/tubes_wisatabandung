package com.example.tubes_new

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class PageAlam : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_page_alam)

        val farmhouse = findViewById<Button>(R.id.btnFarmhouse)
        val kawah = findViewById<Button>(R.id.btnKawah)
        val patenggang = findViewById<Button>(R.id.btnSituPatenggang)
        val parahu = findViewById<Button>(R.id.btnParahu)
        val rancaupas = findViewById<Button>(R.id.btnRancaUpas)
        val tahura = findViewById<Button>(R.id.btnTahura)

        farmhouse.setOnClickListener {
            val intent = Intent(this,AlamFarmhouse::class.java)
            startActivity(intent)
        }
        kawah.setOnClickListener {
            val intent = Intent(this,AlamKawah::class.java)
            startActivity(intent)
        }
        patenggang.setOnClickListener {
            val intent = Intent(this,AlamPatenggang::class.java)
            startActivity(intent)
        }
        parahu.setOnClickListener {
            val intent = Intent(this,AlamPerahu::class.java)
            startActivity(intent)
        }
        rancaupas.setOnClickListener {
            val intent = Intent(this,AlamRancaUpas::class.java)
            startActivity(intent)
        }
        tahura.setOnClickListener {
            val intent = Intent(this,AlamTahura::class.java)
            startActivity(intent)
        }
    }
}