package com.example.tubes_new

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class PageKuliner : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_page_kuliner)

        val batagor = findViewById<Button>(R.id.btnBatagor)
        val lotek = findViewById<Button>(R.id.btnLotek)
        val minuman = findViewById<Button>(R.id.btnMinuman)
        val seblak = findViewById<Button>(R.id.btnSeblak)
        val surabi = findViewById<Button>(R.id.btnSurabi)
        val tahulembang = findViewById<Button>(R.id.btnTahuLembang)

        batagor.setOnClickListener {
            val intent = Intent(this,KulinerBatagor::class.java)
            startActivity(intent)
        }
        lotek.setOnClickListener {
            val intent = Intent(this,KulinerLotek::class.java)
            startActivity(intent)
        }
        minuman.setOnClickListener {
            val intent = Intent(this,KulinerMinuman::class.java)
            startActivity(intent)
        }
        seblak.setOnClickListener {
            val intent = Intent(this,KulinerSeblak::class.java)
            startActivity(intent)
        }
        surabi.setOnClickListener {
            val intent = Intent(this,KulinerSurabi::class.java)
            startActivity(intent)
        }
        tahulembang.setOnClickListener {
            val intent = Intent(this,KulinerTahuLembang::class.java)
            startActivity(intent)
        }
    }
}