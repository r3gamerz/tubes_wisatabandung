package com.example.tubes_new

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class PageBelanja : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_page_belanja)

        val cibaduyut = findViewById<Button>(R.id.btnCibaduyut)
        val btc = findViewById<Button>(R.id.btnBTC)
        val cihampelas = findViewById<Button>(R.id.btnCihampelas)
        val pasbaru = findViewById<Button>(R.id.btnPasarBaru)
        val pvj = findViewById<Button>(R.id.btnPvj)
        val rmode = findViewById<Button>(R.id.btnRumahMode)

        cibaduyut.setOnClickListener {
            val intent = Intent(this,BelanjaCibaduyut::class.java)
            startActivity(intent)
        }
        btc.setOnClickListener {
            val intent = Intent(this,BelanjaBtc::class.java)
            startActivity(intent)
        }
        cihampelas.setOnClickListener {
            val intent = Intent(this,BelanjaCihampelas::class.java)
            startActivity(intent)
        }
        pasbaru.setOnClickListener {
            val intent = Intent(this,BelanjaPasarBaru::class.java)
            startActivity(intent)
        }
        pvj.setOnClickListener {
            val intent = Intent(this,BelanjaPvj::class.java)
            startActivity(intent)
        }
        rmode.setOnClickListener {
            val intent = Intent(this,BelanjaRumahMode::class.java)
            startActivity(intent)
        }
    }
}