package com.example.tubes_new

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class BelanjaPvj : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_belanja_pvj)

        val pvjMap = findViewById<Button>(R.id.btnPVJMap)
        pvjMap.setOnClickListener {
            val gmmIntentUri = Uri.parse("geo:0,0?q=Paris Van Java Resort Lifestyle Place")
            val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
            mapIntent.setPackage("com.google.android.apps.maps")
            startActivity(mapIntent)
        }
    }
}