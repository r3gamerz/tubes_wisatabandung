package com.example.tubes_new

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class SejarahKonfAsiaAfrika : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sejarah_konf_asia_afrika)

        val kaaMap = findViewById<Button>(R.id.btnKAAMap)
        kaaMap.setOnClickListener {
            val gmmIntentUri = Uri.parse("geo:0,0?q=Museum Konferensi Asia Afrika")
            val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
            mapIntent.setPackage("com.google.android.apps.maps")
            startActivity(mapIntent)
        }
    }
}