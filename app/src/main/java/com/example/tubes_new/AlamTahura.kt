package com.example.tubes_new

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class AlamTahura : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_alam_tahura)

        val tahuraMap = findViewById<Button>(R.id.btnTHRMap)
        tahuraMap.setOnClickListener {
            val gmmIntentUri = Uri.parse("geo:0,0?q=Taman Hutan Raya Ir. H. Djuanda")
            val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
            mapIntent.setPackage("com.google.android.apps.maps")
            startActivity(mapIntent)
        }
    }
}