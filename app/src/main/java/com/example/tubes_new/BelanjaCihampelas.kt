package com.example.tubes_new

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class BelanjaCihampelas : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_belanja_cihampelas)

        val cihampelasMap = findViewById<Button>(R.id.btnCHMMap)
        cihampelasMap.setOnClickListener {
            val gmmIntentUri = Uri.parse("geo:0,0?q=Jl. Cihampelas")
            val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
            mapIntent.setPackage("com.google.android.apps.maps")
            startActivity(mapIntent)
        }
    }
}