package com.example.tubes_new

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class BelanjaPasarBaru : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_belanja_pasar_baru)

        val pasarbaruMap = findViewById<Button>(R.id.btnPSBMap)
        pasarbaruMap.setOnClickListener {
            val gmmIntentUri = Uri.parse("geo:0,0?q=Pasar Baru Trade Center")
            val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
            mapIntent.setPackage("com.google.android.apps.maps")
            startActivity(mapIntent)
        }
    }
}