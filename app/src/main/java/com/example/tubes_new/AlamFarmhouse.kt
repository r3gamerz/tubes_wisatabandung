package com.example.tubes_new

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class AlamFarmhouse : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_alam_farmhouse)

        val farmhouseMap = findViewById<Button>(R.id.btnFHLMap)
        farmhouseMap.setOnClickListener {
            val gmmIntentUri = Uri.parse("geo:0,0?q=Farmhouse Lembang")
            val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
            mapIntent.setPackage("com.google.android.apps.maps")
            startActivity(mapIntent)
        }
    }
}