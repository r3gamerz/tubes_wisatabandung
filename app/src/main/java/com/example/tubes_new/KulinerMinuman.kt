package com.example.tubes_new

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class KulinerMinuman : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_kuliner_minuman)

        val minumanMap = findViewById<Button>(R.id.btnMinMap)
        minumanMap.setOnClickListener {
            val gmmIntentUri = Uri.parse("geo:0,0?q=Bajigur Bandrek Two Aa Ahmad Yani")
            val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
            mapIntent.setPackage("com.google.android.apps.maps")
            startActivity(mapIntent)
        }
    }
}