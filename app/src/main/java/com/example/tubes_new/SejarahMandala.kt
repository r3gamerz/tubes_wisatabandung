package com.example.tubes_new

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class SejarahMandala : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sejarah_mandala)

        val mandalaMap = findViewById<Button>(R.id.btnMDLMap)
        mandalaMap.setOnClickListener {
            val gmmIntentUri = Uri.parse("geo:0,0?q=Museum Mandala Wangsit Siliwangi")
            val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
            mapIntent.setPackage("com.google.android.apps.maps")
            startActivity(mapIntent)
        }
    }
}