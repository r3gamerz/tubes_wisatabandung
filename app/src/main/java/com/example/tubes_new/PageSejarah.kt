package com.example.tubes_new

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class PageSejarah : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_page_sejarah)

        val sate = findViewById<Button>(R.id.btnSate)
        val braga = findViewById<Button>(R.id.btnBraga)
        val bla = findViewById<Button>(R.id.btnBLA)
        val mandala = findViewById<Button>(R.id.btnMandala)
        val gugat = findViewById<Button>(R.id.btnIndGugat)
        val asiaafrika = findViewById<Button>(R.id.btnAsiaAfrika)

        sate.setOnClickListener {
            val intent = Intent(this,SejarahSate::class.java)
            startActivity(intent)
        }
        braga.setOnClickListener {
            val intent = Intent(this,SejarahBraga::class.java)
            startActivity(intent)
        }
        bla.setOnClickListener {
            val intent = Intent(this,SejarahBla::class.java)
            startActivity(intent)
        }
        mandala.setOnClickListener {
            val intent = Intent(this,SejarahMandala::class.java)
            startActivity(intent)
        }
        gugat.setOnClickListener {
            val intent = Intent(this,SejarahIndMenggugat::class.java)
            startActivity(intent)
        }
        asiaafrika.setOnClickListener {
            val intent = Intent(this,SejarahKonfAsiaAfrika::class.java)
            startActivity(intent)
        }
    }
}